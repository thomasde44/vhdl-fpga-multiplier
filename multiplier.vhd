

LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
entity nxn is
    
    port(
        clk: in std_logic;
        start: in std_logic;
        mc: in unsigned(7 downto 0);
        mp: in unsigned(7 downto 0);
        ans: out unsigned(15 downto 0);
        ans2: out unsigned(15 downto 0);
        done: out std_logic
    );   
end nxn;

architecture char of nxn is
  --  signal acc: unsigned(15 downto 0);
    signal tmp16: unsigned(15 downto 0):= "0000000000000000";

    signal state: integer range 0 to 4;
    begin

        process(clk, start)
        variable tmp16: unsigned(15 downto 0);
        variable cnt: integer := 0;
        variable addtmp: integer :=0;
        variable utmp: unsigned(7 downto 0);
        begin  
            if clk'event and clk = '1' and (start = '0')  then
                done <= '0';
            case state is
                when 0 =>
                    tmp16 := "00000000" & mp;
                    state <= 1;
                when 1 =>
                    if tmp16(0 downto 0) = 0 then
                        state <= 2;
                    else 
                        state <= 3;
                    end if;
                when 2 =>
                    tmp16 := '0' & tmp16(15 downto 1);
                    cnt := cnt + 1;
                    if(cnt = 8) then
                        state <= 4;
                    else
                    state <= 1;
                    end if;
                when 3 =>
                    tmp16(15 downto 8) := tmp16(15 downto 8)+ mc;
                    tmp16 := '0' & tmp16(15 downto 1);
                    cnt := cnt + 1;
                    if(cnt = 8) then
                        state <= 4;
                    else
                    state <= 1;
                    end if;
                when 4 =>
                    ans <= tmp16;
                    done <= '1';
                    state <= 0;
                end case;
             end if;
        end process;
end char;